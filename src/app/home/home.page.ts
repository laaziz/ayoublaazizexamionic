import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as Firebase from 'firebase';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertController} from '@ionic/angular';
import {CameraPreview, CameraPreviewDimensions, CameraPreviewOptions, CameraPreviewPictureOptions} from '@ionic-native/camera-preview/ngx';
import { ImagePicker } from '@ionic-native/image-picker';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  // tslint:disable-next-line:ban-types
  hideLogin: Boolean = true;
  // tslint:disable-next-line:ban-types
  hideRegister: Boolean = false;
  loginForm: FormGroup;
  registerForm: FormGroup;

  // tslint:disable-next-line:max-line-length
  // @ts-ignore
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, public router: Router, private formBiulder: FormBuilder, public alertController: AlertController){
    this.loginForm = this.formBiulder.group({
      usernameLogin: [null, Validators.required],
      passwordLogin: [null, Validators.required]
    });
    this.registerForm = this.formBiulder.group({
      firstname: [null, Validators.required],
      email: [null, Validators.required],
      name: [null, Validators.required],
      gender: [null, Validators.required],
      city: [null, Validators.required],
      phone: [null, Validators.required],
      usernameSignup: [null, Validators.required],
      passwordSignup: [null, Validators.required]
    });
  }

  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
    if (ev?.detail?.value === 'register') {
      this.hideRegister = true;
      this.hideLogin = false;
    } else {
      this.hideRegister = false;
      this.hideLogin = true;
    }
  }

   login(){
    const ref = Firebase.database().ref('user/');
    ref.once('value', (snapshot) => {
      // @ts-ignore
      snapshot.forEach(async (childSnapshot) => {
        const item: any = childSnapshot.toJSON();
        if (this.loginForm.value.usernameLogin === item.usernameSignup && this.loginForm.value.passwordLogin === item.passwordSignup) {
          this.router.navigate(['/product-list']);
        } else {
          const alert = await this.alertController.create({
            header: 'Error',
            message: 'Usernname & password are required',
            buttons: ['OK']
          });
          await alert.present();
        }
      });
    });
  }

  choosePic() {
    ImagePicker.getPictures({
      maximumImagesCount : 1
    }).then(data => {

    }).catch(err => {
      alert(err);
    });
  }

  register() {
    const user = Firebase.database().ref('user/').push();
    user.set(this.registerForm.value);
    this.router.navigate(['/product-list']);
  }


}
