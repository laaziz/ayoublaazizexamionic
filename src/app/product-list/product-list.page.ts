import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import * as Firebase from 'firebase';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.page.html',
  styleUrls: ['./product-list.page.scss'],
})
export class ProductListPage implements OnInit {
  items = [];
  constructor(private router: Router){}

  ngOnInit() {
    const ref = Firebase.database().ref('product/');
    ref.once('value', (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        this.items.push(childSnapshot.toJSON());
      });
    });
  }

  addProduct(){
    this.router.navigate(['/add-product']);
  }

  goToProductDetails(item: JSON) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        item
      }
    };
    this.router.navigate(['/product-details'], navigationExtras);
  }
}
