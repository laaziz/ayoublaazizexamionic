import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import * as Firebase from 'firebase';

let firebaseConfig = {
  apiKey: 'AIzaSyBdMMOhDV2joPsOSNC2w2CQHO2NRXswJns',
  authDomain: 'examlaazizayoub.firebaseapp.com',
  databaseURL: 'https://examlaazizayoub.firebaseio.com',
  projectId: 'examlaazizayoub',
  storageBucket: 'examlaazizayoub.appspot.com'
};

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    Firebase.initializeApp(firebaseConfig);
  }
}
