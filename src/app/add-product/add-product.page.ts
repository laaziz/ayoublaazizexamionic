import { Component, OnInit } from '@angular/core';
import * as Firebase from 'firebase';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {NavController} from '@ionic/angular';
import {ImagePicker} from '@ionic-native/image-picker';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.page.html',
  styleUrls: ['./add-product.page.scss'],
})
export class AddProductPage implements OnInit {
  addProduct: FormGroup;
  constructor(public router: Router, private formBuilder: FormBuilder, public navCtrl: NavController) {
    this.addProduct = this.formBuilder.group({
      libelle: [null, Validators.required],
      code: [null, Validators.required],
      desc: [null, Validators.required],
      price: [null, Validators.required],
      priceBuy: [null, Validators.required],
      brand: [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  addProductAction() {
    const ref = Firebase.database().ref('product/').push();
    ref.set(this.addProduct.value);
    this.navCtrl.pop();
  }

  choosePic() {
    // @ts-ignore
    ImagePicker.getPictures( {
      maximumImagesCount: 1
    }).then(res => {
      console.log(res);
    }).catch(err => {
      alert(err);
    });
  }

}
