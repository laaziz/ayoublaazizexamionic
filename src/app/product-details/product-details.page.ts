import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NavParams} from '@ionic/angular';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {
  getValue = {};

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe((params) => {
      let navParams = this.router.getCurrentNavigation().extras;
      this.getValue = navParams.queryParams.item;
      console.log(navParams);
    });
  }

  ngOnInit() {
  }

}
